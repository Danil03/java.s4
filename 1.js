function count(){
let numberOne = prompt('Enter first number')

let num1 = parseInt(numberOne)

let numberTwo = prompt('Enter second number')

let num2 = parseInt(numberTwo)

let operate = prompt('Operator(/, *, +, -)')

if(!isNaN (num1) && !isNaN(num2) && operate){
    let result;
    switch (operate){
        case '+':
        result = num1 + num2;
        break;

        case'-':
        result = num1 - num2;
        break;

        case '/':
        result = num1 / num2;
        break
        
        case '*':
        result = num1 * num2
    }
    console.log(result)
  }
}
count()

// ВІДПОВІДІ НА ПИТАННЯ:
// 1.Функції потрібні для того щоб не повторювати один і той самий код по декілька разів, 
// а наприклад просто прописати його один раз і викликати його по назві яку ми їй дали.

// 2.Коли аргумент передається у функцію за значенням, зміна значення аргументу всередині функції
//  не впливає на його значення поза функцією. Аргументи дозволяють передавати конкретні дані чи значення в функцію. 
//  Це дозволяє функції працювати зі змінними та даними, які можуть змінюватися в різних контекстах.

// 3."return" у функції може повернути результат, який буде переданий у код, що її викликав. Як тільки
//  виконання доходить до місця де встановлений return, функція зупиняється, і значення повертається.





// function numberOne() {

//     prompt ('Enter first number')
// }

// function numberTwo() {
//     prompt ('Enter second number')
// }


// numberOne()
// numberTwo()